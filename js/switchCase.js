function determinesDateDay(entry) {
  var result = null
  var date = new Date (entry.value)
  var day = date.getDay()

  switch(day) {
    case 0:
      result = "The day is Monday"
      break
    case 1:
      result = "The day is Tuesday"
      break
    case 2:
      result = "The day is Wednesday"
      break
    case 3:
      result = "The day is Thursday"
      break
    case 4:
      result = "The day is Friday"
      break
    case 5:
      result = "The day is Saturday"
      break
    case 6:
      result = "The day is Sunday"
      break
    default:
      result = "Date value incorrect"
  }

  document.getElementById("result").innerHTML = result
}