// 5! = 1*2*3*4*5 = 120
function recursiveFuntion(param) {
  var data = param.value
  
  if (data >= 0 && data <= 100) {
    var result = factorial(data)
    clean()
    toPrint(result)
  } else {
    clean()
    toPrint("Incorrect value. Only accepts values between 0 and 100")
  }
}

function factorial(num) {
  if (num == 0 || num == 1)
    return 1
  else
    return num * factorial(num - 1)
}

function toPrint(value) {
  var result = document.getElementById("result")
  result.innerHTML = "Result: " + value
}

function clean() {
  document.getElementById("result").innerHTML = ""
}