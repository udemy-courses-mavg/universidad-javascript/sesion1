document.write("<h1>Data type management program</h1><br/>")

// String
document.write("STRING<br/>")
var string = "Hi from JS!"
document.write("String value: " + string + "<br/><br/>")


// Numbers
document.write("NUMBERS<br/>")
var num1 = 15
var num2 = parseInt("25")
var num3 = num1 + num2

document.write("Num1 value: " + num1 + "<br/>")
document.write("Num2 value: " + num2 + "<br/>")
document.write("Num3 value: Num1 + Num2 = " + num3 + "<br/><br/>")


// Boolean 
document.write("BOOLEAN<br/>")
var flag = true
var result = (num1 == num2)

document.write("Flag value: " + flag + "<br/>")
document.write("Result value: " + result + "<br/><br/>")


// Null & Undefined
document.write("NULL & UNDEFINED<br/>")
var nullType = null
var undefinedType

document.write("Null value: " + nullType + "<br/>")
document.write("Undefined value: " + undefinedType + "<br/><br/>")


// Know data type
document.write("KNOW DATA TYPE<br/>")
result = (typeof num1 == "number");
document.write("Num1 var is number? " + result + "<br/>")
result = (typeof string == "string");
document.write("String var is string type? " + result + "<br/>")
result = (typeof flag == "boolean");
document.write("Flag var is boolean? " + result + "<br/><br/>")
