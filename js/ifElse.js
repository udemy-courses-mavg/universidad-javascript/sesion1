function ageClasification(param) {
  var result = null
  var age = param.value

  if (age > 0 && age < 12)
    result = "AA & A"
  else if (age >= 12 && age < 15)
    result = "B, AA & A"
  else if (age >= 15 && age < 18)
    result = "B15, AA & A"
  else if (age >= 18 && age < 21)
    result = "C, B15, AA & A"
  else if (age >= 21 && age < 122) // The person more long-lived had 122 years
    result = "You can watch any movie type"
  else 
    result = "The value inserted is incorrect. Please try again"

  document.getElementById("result").innerHTML = result
}