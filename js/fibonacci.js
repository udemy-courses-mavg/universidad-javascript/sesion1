// 0, 1, 1, 2, 3, 5, 8, 13, 21, ...
function successionFibonacci(param) {
  clean()
  var actual = 0
  var newValue = 1
  var next = null
  var maxElements = 100
  var serieElements = param.value
  
  if (serieElements > maxElements) {
    toPrint("Try with less elements")
    return
  }

  toPrint(actual)
  toPrint(newValue)

  for (i=0; i<serieElements-2; i++) {
    next = actual + newValue
    actual = newValue
    newValue = next
    toPrint(next)
  }
}

function toPrint(value) {
  document.getElementById("result").innerHTML += value + " "
}

function clean() {
  document.getElementById("result").innerHTML = ""
}