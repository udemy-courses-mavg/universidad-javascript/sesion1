function matrixManagement() {
  /* Matrix definition 
    [a b c]
    [d e f]
    [g h i]
  */
  var data = null
  var a1 = new Array('a', 'b', 'c')
  var a2 = new Array('d', 'e', 'f')
  var a3 = new Array('g', 'h', 'i')

  var a = new Array(a1, a2, a3)

  for (i=0; i<a.length; i++) {
    toPrint("[ ")
    for (j=0; j<a[i].length; j++) {
      data = a[i][j]
      toPrint(data)
    }    
    toPrint(" ]" + "<br/>")
  }

}

function toPrint(data) {
  document.getElementById("result").innerHTML += data + " "
}

function clean() {
  document.getElementById("result").innerHTML = ""
}