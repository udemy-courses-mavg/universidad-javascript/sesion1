function generateEvenNumbers() {
  var limit = 10
  var data = 0
  while (data <= limit) {
    if (data % 2 == 0)
      document.getElementById("result").innerHTML += data + " "
    data ++
  }
}

function clean() {
  document.getElementById("result").innerHTML = ""
}