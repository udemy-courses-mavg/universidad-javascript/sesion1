document.write("<h1>Operators Management Program</h1><br/>");

// Operator '=' asigns values
var x = 5
var y
y = 6
document.write("Value of x = " + x + "<br/>")
document.write("Value of y = " + y + "<br/><br/>")

// Operator '+' adds values or concat stringsvar 
var z = x + y
document.write("Value of z = " + z + "<br/><br/>")
var a = "Hi"
var b = "World!!"
document.write("Value of a = " + a + "<br/>")
document.write("Value of b = " + b + "<br/>")
document.write("Strings concat (a+b) = " + a + " " + b + "<br/><br/>")

// Operator '++' increase in one the value assign
x++
document.write("The value of x++ = " + x + "<br/><br/>")


// Operator '--' decrease in one the value assign
y--
document.write("The value of y-- = " + y + "<br/><br/>")

// Operators '+=', '-=', '*=', '/=', etc; reduce the syntax
x += y
document.write("'x += y' = " + x + "<br/>")
x -= y
document.write("'x -= y' = " + x + "<br/>")
x *= y
document.write("'x *= y' = " + x + "<br/>")
x /= y
document.write("'x /= y' = " + x + "<br/><br/>")

