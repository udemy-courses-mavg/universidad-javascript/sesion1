var name = null // Global variable

function showName(param) {
  name = param.value
  var greeting = "Hi " + name.toUpperCase()
  
  document.getElementById("result").innerHTML = greeting
}

function greetLocal(param) {
  var name = param
  alert("Hi " + name + " (local)")
  alert("Hi " + this.name + " (global)")
}

function greetGlobal() {
  alert("Hi " + name)
}
